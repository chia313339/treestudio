import requests
from bs4 import BeautifulSoup as bs
import json


api_url = "http://10.95.43.73:8001/api.php"
index_url = "http://10.95.43.73:8001/index.php"

def get_login_token(raw_resp):
    soup = bs(raw_resp.text)
    token = [n.get('value', '') for n in soup.find_all(
        'input') if n.get('name', '') == 'wpLoginToken']
    return token[0]


# create wiki session
def wiki_session(index_url):
    payload = {
        'wpName': 'BotEdit',
        'wpPassword': 'BotEdit202010',
        'wploginattempt': 'Log in',
        'wpEditToken': "+\\",
        'title': "Special:UserLogin",
        'authAction': "login",
        'force': "",
        'wpForceHttps': "1",
        'wpFromhttp': "1",
    }
    sess = requests.session()
    resp = sess.get(index_url + '?title=特殊:使用者登入&returnto=Spezial%3AUserLogin')
    payload['wpLoginToken'] = get_login_token(resp)
    response_post = sess.post(
        index_url + '?title=Spezial:UserLogin&action=submitlogin&type=login', data=payload)
    return sess


# get wiki recentchanges last N days
def wiki_recentchanges(sess, index_url, api_url):
    PARAMS = {
        "action": "query",
        "format": "json",
        "list": "recentchanges",
        "utf8": 1,
        "rcprop": "user|userid|comment|title|timestamp|ids|sizes",
        "rclimit": 100,
        "enhanced": 1,
        "urlversion": 2
    }
    wiki_recent = []
    last_title = ''
    wiki_no = 0
    datalist = sess.get(url=api_url, params=PARAMS).json()[
        'query']['recentchanges']
    for tmp in datalist:
        if (tmp['user'] != 'BotEdit' and tmp['title'] != last_title and tmp['type'] == 'edit'):
            tmp_list = {
                "title": "{}".format(tmp['title']),
                "user": "{}".format(tmp['user']),
                "url": index_url + "/{}".format(tmp['title']),
                "date": "{}".format(tmp['timestamp'][:10])
            }
            last_title = tmp['title']
            wiki_recent.append(tmp_list)
            wiki_no += 1
        if wiki_no > 20:
            break
    return wiki_recent



try:
    sess = wiki_session(index_url)
    w_list = wiki_recentchanges(sess, index_url, api_url)
except:
    print('wiki server not found!!')
    w_list = ''
    
    
jsonString = json.dumps(w_list,ensure_ascii=False)
print(jsonString)

with open('data.txt', 'w', encoding='utf-8') as f:
    json.dump(jsonString, f, ensure_ascii=False, indent=4)
